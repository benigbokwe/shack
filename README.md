Simple API built to resolve issues with Bob's [Task 1](https://github.com/flockcover/tech-screen-instructions/blob/master/Task1.md) API.

The project is intented to refine and fine-tune the API and provide a client side application to consume the API. Due to limitation in time, I was able to build just the API side and will be improving it as well as developing the client side with React/Redux in the future;

## Instructions
Clone repository

```
git clone https://benigbokwe@bitbucket.org/benigbokwe/shack.git
```

Start API server
```
cd api/ && npm i && npm run dev
```

If you have other node isntances running, you can kill all running processes using this command

```
killall -9 node
```

Runs the app in the development mode.<br>
Open [http://localhost:3400](http://localhost:3400) to view it in the browser.
Open [All drones](http://localhost:3400/api/v0/drones) to view all drones
Open [Single drone](http://localhost:3400/api/v0/drone/1) to view a single drone response

No tests are added at this point unfornately but I'll ensure that all the units are well tested in the nearest future

For production build, run
```
cd npm run build:prod
```

## Tech stack

*  Node JS v8 
*  Express v4
*  Node cron
*  Redis
*  Babel

### What assumptions did you have to make? 
Errors are randomly generated on Bob's API server. After about 1-10 request retries, it generates a succeful response back to the requestor.
I also assumed that all responses from api endpoint is retrieved from a single data store - all single drone data is an element/object of drones array.

### Which technologies did you choose? Why?
I decided to settle for the technologies listed in `Tech stack` section for the following reasons:

`Node js` �  Open source platform for web apps with large community support. It runs single threaded which is a helpful limitation as it simplifies  how we can code without worrying about concurrency issues. And above all, it's fast;

`Express js`: Its fast, un-opinionated and very minimalist web framework for node with great features � robust routing, focuses on high performance, https helpers and many more;

`Redis` REDIS offers a framework for building in memory application that is at the same time versatile and scalable; it offers a great deal of support for developing efficient caching mechanism. It's easy to implement and the outcome is a high-performing cache system;

`Babel` Of all the ES6 transpilers, Babel has the greatest level of compatibility with the ES6 spec. It virtually let's you use all the few features of ES6.

### What technical compromises did you have to make in order to achieve your solution? What is the severity of this tech debt, and what would a path to resolving it look like?
Initially, I settled on allowing error response to be sent back to the requestor if after retrying api calls for 5 times and received no valid response and also if no data is previously stored in my redis cache. 

This solution was great but still did not tackle the underlying issue with Bob's server. Secondly, from Restful service standpoint, service calls should be idempotent - if clients make the same calls repeatedly, the same outout should be provided. And this wasn't resolved either. To solve this issue and limit the amount of network requests sent to underperformant Bob's machine, I decided to introduce a task scheduler which runs every 10 minutes, fetches data from the api endpoint and updates the redis store. All subsequent API calls to my machine will be handled by the node server - results are fetched from the redis store and served to the client. However, if there's no saved data in the redis store, an api call is made to the upstream endpoint (Bob's server) to repopulate the store if successful response was received. 

