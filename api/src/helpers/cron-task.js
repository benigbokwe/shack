import {redisClient} from './redis-client'
import {fetchHandler} from './request-handler'

export const taskScheduler = () => {
    console.log('running a task every 10 minute')

    return fetchHandler(process.env.DRONES_API_END_POINT, 10)
        .then(res => res.json())
        .then(json => {
            // Save data in redis store to expire after one hour
            redisClient.setex(process.env.SHACK_DRONES_REDIS_KEY, 60 * 60, JSON.stringify(json))
        })
        .catch((err) => {
            console.error('Error: can no longer be handled after multi retries', err)
        })
}