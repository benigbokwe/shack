class HTTPError extends Error {
    constructor(message, status) {
      super(message)
      this.name = "HTTPError"
      this.status = status
    }
}

export default HTTPError