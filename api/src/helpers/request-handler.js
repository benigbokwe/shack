import fetch from 'node-fetch'
import HTTPError from './error-handler'
import {redisClient} from './redis-client'

/**
 * 
 * @param {object} res
 */
const checkStatus = (res) => {
  if (res.ok) {
      return res
  }

  throw new HTTPError(res.statusText, res.status)
}

/**
 * Fetch handler - Retries request on failure
 * 
 * @param {string} url 
 * @param {number} limit
 * @param {object} options 
 */
export const fetchHandler = (url, limit, options) => {
  console.log(`Handling fetch request: ${url} - ${limit}`)
  return fetch(url, options)
    .then(checkStatus)
    .catch((err) => {
      if (limit === 1) throw err
      // @TODO SET a timer to prevent constant api requests
      return fetchHandler(url, limit - 1, options)
    })
}

const getRequestResponse = (response, routeId) => {
    // handles single queries  - api/v0/drone/1
    if (routeId) {
        const filteredRes =  response.filter((item) => (
            item.droneId === routeId
        ))

      return filteredRes
    }

  return response
}

/**
 * Handles external api request
 * @param {string} url 
 */
export const handleFetchExternalData = (req, res) => {
  console.log(`Handling external request: ${req.path}`)

  //const url = process.env.API_END_POINT + req.path
  const searchKey = process.env.SHACK_DRONES_REDIS_KEY

  // check request type - full or paramiterized
  const routeId = Number(req.params.id)

  redisClient.get(searchKey, (err, response) => {
    if (err || response === null) {
      return fetchHandler(process.env.DRONES_API_END_POINT, 5)
        .then(res => res.json())
        .then(json => {
            // Expire after one hour
            redisClient.setex(searchKey, 60 * 60, JSON.stringify(json))
            console.log('=== Serve result from API response ===')
            return res.json(getRequestResponse(json, routeId))
        })
        .catch((err) => {
            console.error('Error: can no longer be handled after 5 retries')
            res.status(err.status).json(err.message)
      })
    } else {
        console.log('=== Serve result from Redis Store ===')
        return res.send(getRequestResponse(JSON.parse(response), routeId))
    }
  })
}
