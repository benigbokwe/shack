// import "@babel/polyfill"
import express from 'express'
import compression from 'compression'

import cron from 'node-cron'
require('dotenv').config()
import {handleFetchExternalData} from './helpers/request-handler'
import {taskScheduler} from './helpers/cron-task'

const { PORT } = process.env
const app = express()

// Run task scheduler in the background every 10 minutes to 
// populate redis data store
cron.schedule('*/10 * * * *', taskScheduler)

// compress all responses
app.use(compression())

app.get('/api/v0/drones', handleFetchExternalData)
app.get('/api/v0/drone/:id', handleFetchExternalData)
app.use((req, res, next) => {
    res.status(404).send("Sorry can't find that!")
})

// start the server
app.listen(PORT, (error) => {
    if (error) throw error
    console.log(`> Server running on http://localhost:${PORT}`)
})